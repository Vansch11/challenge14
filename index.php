<?php

interface Printable {
    public function print();
    public function sneakpeek();
    public function fullinfo();
}

class Furniture implements Printable {
    private $width;
    private $length;
    private $height;
    private $is_for_seating;
    private $is_for_sleeping;

    public function __construct($width, $length, $height) {
        $this->width = $width;
        $this->length = $length;
        $this->height = $height;
    }

    public function getWidth() {
        return $this->width;
    }

    public function getLength() {
        return $this->length;
    }

    public function getHeight() {
        return $this->height;
    }

    public function getIsForSeating() {
        return $this->is_for_seating;
    }

    public function setIsForSeating($is_for_seating) {
        $this->is_for_seating = $is_for_seating;
    }

    public function getIsForSleeping() {
        return $this->is_for_sleeping;
    }

    public function setIsForSleeping($is_for_sleeping) {
        $this->is_for_sleeping = $is_for_sleeping;
    }

    public function area() {
        return $this->width * $this->length;
    }

    public function volume() {
        return $this->area() * $this->height;
    }

    public function print() {
        echo get_class($this) . ", ";
        if ($this->is_for_seating && $this->is_for_sleeping) {
            echo "suitable for seating and sleeping";
        } elseif ($this->is_for_seating) {
            echo "suitable for seating only";
        } elseif ($this->is_for_sleeping) {
            echo "suitable for sleeping only";
        } else {
            echo "not suitable for seating or sleeping";
        }
        echo ", " . $this->area() . "cm2<br>";
    }

    public function sneakpeek() {
        echo get_class($this) . "<br>";
    }

    public function fullinfo() {
        echo get_class($this) . ", ";
        if ($this->is_for_seating && $this->is_for_sleeping) {
            echo "suitable for seating and sleeping";
        } elseif ($this->is_for_seating) {
            echo "suitable for seating only";
        } elseif ($this->is_for_sleeping) {
            echo "suitable for sleeping only";
        } else {
            echo "not suitable for seating or sleeping";
        }
        echo ", " . $this->area() . "cm2, ";
        echo "width: " . $this->width . "cm, ";
        echo "length: " . $this->length . "cm, ";
        echo "height: " . $this->height . "cm<br>";
    }
}

class Sofa extends Furniture {
    private $seats;
    private $armrests;
    private $length_opened;

    public function __construct($width, $length, $height, $seats, $armrests, $length_opened) {
        parent::__construct($width, $length, $height);
        $this->seats = $seats;
        $this->armrests = $armrests;
        $this->length_opened = $length_opened;
        $this->setIsForSeating(true);
        $this->setIsForSleeping(false);
    }

    public function getSeats() {
        return $this->seats;
    }

    public function setSeats($seats) {
        $this->seats = $seats;
    }

    public function getArmrests() {
        return $this->armrests;
    }

    public function setArmrests($armrests) {
        $this->armrests = $armrests;
    }

    public function getLengthOpened() {
        return $this->length_opened;
    }

    public function setLengthOpened($length_opened) {
        $this->length_opened = $length_opened;
    }

    public function area_opened() {
        if ($this->getIsForSleeping()) {
            return $this->getWidth() * $this->length_opened;
        } else {
            return "This sofa is for sitting only, it has {$this->armrests} armrests and {$this->seats} seats.";
        }
    }
}

class Bench extends Sofa {
    public function __construct($width, $length, $height, $seats, $armrests, $length_opened) {
        parent::__construct($width, $length, $height, $seats, $armrests, $length_opened);
    }
}

class Chair extends Furniture {
    public function __construct($width, $length, $height) {
        parent::__construct($width, $length, $height);
        $this->setIsForSeating(true);
        $this->setIsForSleeping(false);
    }
}


$furniture = new Furniture(200, 100, 50);
echo "Area: " . $furniture->area() . "cm2<br>";
echo "Volume: " . $furniture->volume() . "cm3<br>";
$furniture->print();
$furniture->sneakpeek();
$furniture->fullinfo();


$sofa = new Sofa(150, 200, 100, 3, 2, 0);
echo "Area: " . $sofa->area() . "cm2<br>";
echo "Volume: " . $sofa->volume() . "cm3<br>";
echo "Area Opened: " . $sofa->area_opened() . "<br>";


$sofa->setIsForSleeping(true);
$sofa->setLengthOpened(180);


echo "Area Opened: " . $sofa->area_opened() . "<br>";


$bench = new Bench(180, 150, 80, 2, 0, 200);
$bench->print();
$bench->sneakpeek();
$bench->fullinfo();


$chair = new Chair(60, 60, 100);
$chair->print();
$chair->sneakpeek();
$chair->fullinfo();


?>
